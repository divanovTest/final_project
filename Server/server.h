/*!
    @file
    @brief header file , who contains functions and definitions
*/
#ifndef SERVER_H
#define SERVER_H

#define BUFF_SIZE 512
#define PORT_NUM_SERVER 1234
#define LOGS_FILE_NAME "logs"
#define TIMEOUT_CLIENT_SEC 25
#define TIMEOUT_SERVER_SEC 1500

#include <cstring>
//#define UNIT_TEST
//#define DEBUG
//#define DEBUG_PROTOCOL

pthread_mutex_t mutex;

int sendall(int sock, std::deque<unsigned char> *buf)
{
    int res = 0;
    int len = buf->size();
    char * msgPtr = new char[len + 1];int j =0;
    for(int i = 0; i < len; i++)
    {
        if((*buf)[i] != '\0'){
            msgPtr[j] = (*buf)[i];
            j++;
            if((*buf)[i] == '>')
            {
                msgPtr[i + 1] = '\0';
                break;
            }
        }
    }

    int offset = 0;

    while ((res = send(sock, msgPtr+offset, len < BUFF_SIZE ? len : BUFF_SIZE, 0)) > 0 )
    {
        offset += res;
        len -= res;
        if(len < BUFF_SIZE || res < BUFF_SIZE || res == len)break;
    }
    delete[] msgPtr;
    return (res == -1 ? -1 : offset);
}

int recvall(int sock, std::vector<unsigned char>* buf)
{
    int res = 0;
    int total = 0;
    char recv_buffer[BUFF_SIZE];
    buf->clear();

    while ((res = recv(sock, recv_buffer, BUFF_SIZE, MSG_NOSIGNAL)) > 0)
    {
        char* pointer = recv_buffer;
        int result = res;
        while(result > 0)
        {
            buf->push_back(*pointer);
            result--;
            pointer++;
        }
        total += res;
        if(res < BUFF_SIZE)break;
    }
    return (res == -1 ? -1 : total);
}

void write_logs(std::string *str)
{
    std::ofstream file;
    file.open(LOGS_FILE_NAME,  std::fstream::app | std::fstream::out);
    file << *str;
    file.close();
}

int split(unsigned char splitter, std::vector<unsigned char> *vecCont, std::map<std::string, std::string> *container, std::vector<std::vector<unsigned char>> *vec,
          std::map<std::string, long int> *clients_uptime, std::string splittterS)
{
    std::vector<unsigned char>::iterator pos;
    if(splitter != 0)pos = std::find(vecCont->begin(), vecCont->end(), splitter);
    else
    {
        const char *crlf2 = splittterS.c_str();
        pos = std::search(vecCont->begin(), vecCont->end(), crlf2, crlf2 + strlen(crlf2));
    }

    std::vector<unsigned char> head(std::distance(vecCont->begin(), pos));
    std::copy(vecCont->begin(), pos, head.begin());
    std::vector<unsigned char> tail(std::distance((splitter != 0 ? pos : pos + splittterS.length()), vecCont->end()));
    std::copy((splitter != 0 ? pos + 1 : pos + splittterS.length() + 1), vecCont->end() + 1, tail.begin());
    std::string strHead(head.begin(), head.end());
    std::string strTail(tail.begin(), tail.end() - 1);

    //std::cout << "strTail=" << strTail << std::endl;

    if (container != nullptr && clients_uptime != nullptr)
    {
        long int timestamp = static_cast<long int>(std::time(0));
        if(container->count(strHead))//exist user
        {
             container->at(strHead) = strTail;
             clients_uptime->at(strHead) = timestamp;
        }
        else//new user
        {
            container->insert(std::pair<std::string, std::string>(strHead, strTail));
            clients_uptime->insert(std::pair<std::string, long int>(strHead, timestamp));
            std::string log = "New user: " + strHead + " listen port: " + strTail + ". Connection timestamp: " + std::to_string(timestamp) + "\n\r";
            write_logs(&log);
        }
    }
    if (vec != nullptr)
    {
        vec->clear();
        vec->push_back(head);
        vec->push_back(tail);
    }
    return 0;
}

enum command
{
    REFRESH_PORT = 0,
    REGISTRATION,
    SEND_MESSAGE,
    TEST_PROTOCOL
};

struct containers
{
    std::map<std::string, std::string>* clients;
    std::map<std::string, long int>* clients_uptime;
    int* sock;
    std::set<int>* speakers;
    fd_set* readset;
};

void* update_users_map(void* cont)
{
    containers* Cont = (containers*)cont;
    std::map<std::string, std::string> *clients = (std::map<std::string, std::string>*)Cont->clients;
    std::map<std::string, long int> *clients_uptime = (std::map<std::string, long int>*)Cont->clients_uptime;
    while (1)
    {
        long int timestamp = static_cast<long int>(std::time(0));
        for (std::map<std::string, std::string>::iterator it = (*clients).begin(); it != (*clients).end(); it++)
        {
            if ((*clients_uptime).count(it->first))
            {
                if (abs((*clients_uptime)[it->first] - timestamp) > TIMEOUT_CLIENT_SEC)//client connection lost
                {
                    auto iter_cl_upt = (*clients_uptime).find(it->first);
                    auto iter_cl = (*clients).find(it->first);
                    if (iter_cl_upt != (*clients_uptime).end() && iter_cl != (*clients).end())
                    {
                        (*clients_uptime).erase(iter_cl_upt);
                        (*clients).erase(iter_cl);
                        std::string log = "User: " + it->first + " disconnected. Disconnection timestamp: " + std::to_string(timestamp) + "\n\r";
                        write_logs(&log);
                    }
                    else
                    {
                        std::cout << "\033[1;31mSomething went wrong\033[0m" << std::endl;
                    }

                }
            }
        }
        sleep(5);
    }
    pthread_exit(0);
}

void* listener_thread(void* params)
{
    std::vector<unsigned char> buffer;
    std::vector<std::vector<unsigned char>> t_cont; //!container to keep command's
    std::deque<unsigned char> vec_echo;//!vector keep echo
    std::string t_str;
    Message* mess;
    MyProtocolHeder prot;

    containers* Cont = reinterpret_cast<containers*>(params);
    std::map<std::string, std::string>* clients = reinterpret_cast<std::map<std::string, std::string>*>(Cont->clients);
    std::map<std::string, long int>* clients_uptime = reinterpret_cast<std::map<std::string, long int>*>(Cont->clients_uptime);
    int sock = *((int*)Cont->sock);

    fd_set* readset = reinterpret_cast<fd_set*>(Cont->readset);

    pthread_mutex_lock(&mutex);
    std::set<int>* speakers = reinterpret_cast<std::set<int>*>(Cont->speakers);
    for (std::set<int>::iterator it = speakers->begin(); it != speakers->end(); it++)//!all connected socket's in loop
    {
        if (FD_ISSET(*it, readset))//!if flag on socket is enabled (if any changes was on socket)
        {
            if (recvall(*it, &buffer) <= 0)//!read data in buffer
            {
                close(*it);//!close socket

                speakers->erase(*it);//!erase socket from container

                continue;
            }
            //!Below is the message parsing and response to the client:
            std::vector<unsigned char>::iterator posl = std::find(buffer.begin(), buffer.end(), (unsigned char)'<');
            std::vector<unsigned char>::reverse_iterator posr = std::find(buffer.rbegin(), buffer.rend(), (unsigned char)'>');

            if (posl > buffer.end() || posr.base() > buffer.end())
            {
                vec_echo.clear();
                t_str = "Incorrect command format:\n";
                std::copy(buffer.begin(), buffer.end(), std::back_inserter(t_str));
                std::copy(t_str.begin(), t_str.end(), std::back_inserter(vec_echo));
            }
            else
            {
                std::vector<unsigned char> t_vec;
                std::copy(posl + 1, posr.base() - 1, std::back_inserter(t_vec));
                buffer.clear();
                buffer = t_vec;
            }

            //get command code
            if (split('=', &buffer, nullptr, &t_cont, nullptr, "") < 0)
            {
                std::cout << "\033[1;31msplit() error\033[0m" << std::endl;
                pthread_exit((void*)1);
            }

            buffer = t_cont.at(1);
            int com = stoi(std::string(t_cont.at(0).begin(), t_cont.at(0).end()));

//std::cout<<"com="<<com<<" buffer:"<<std::string(buffer.begin(), buffer.end())<<std::endl;
            switch (com)
            {
            case REFRESH_PORT:


                //print_buf("buffer 0:", &buffer);
                if(!std::memcpy(&prot, reinterpret_cast<void*>(buffer.data()) , sizeof(MyProtocolHeder)))
                {
                    std::cout << "TEST_PROTOCOL memcpy 1 error: " << errno << std::endl;
                };
                vec_echo.clear();

                //!this is message:
                std::copy(buffer.begin() + sizeof(MyProtocolHeder), buffer.begin() + sizeof(MyProtocolHeder) + prot.Size + 1, back_inserter(vec_echo));

                buffer.clear();
                std::copy(vec_echo.begin() , vec_echo.end() , back_inserter(buffer));

                //std::cout << "buffer=" << std::string(buffer.begin(), buffer.end()) <<std::endl;

                //!split message and add/edit record in map:
                if (split(':', &buffer, clients, nullptr, clients_uptime, "") < 0)
                {
                    std::cout << "\033[1;31msplit() error\033[0m" << std::endl;
                    pthread_exit((void*)1);
                }
                buffer.clear();
                t_cont.clear();
                vec_echo.clear();
                //!make from map string and answer to the client
                for (std::map<std::string, std::string>::iterator it = clients->begin(); it != clients->end(); it++)
                {
                    t_str = it->first + ":" + it->second + "|";
                    //std::cout << "t_str=" << t_str << std::endl;
                    std::copy(t_str.begin(), t_str.end(), std::back_inserter(vec_echo));
                }

                //std::cout << "vec_echo=" << std::string(vec_echo.begin(), vec_echo.end()) <<std::endl;

                break;
            case REGISTRATION:
                if (clients->count(std::string(buffer.begin(), buffer.end())))
                {
                    vec_echo.clear();
                    t_str = "Bad username";
                    std::copy(t_str.begin(), t_str.end(), std::back_inserter(vec_echo));
                }
                else
                {
                    vec_echo.clear();
                    t_str = "OK";
                    std::copy(t_str.begin(), t_str.end(), std::back_inserter(vec_echo));
                }
                break;
            case SEND_MESSAGE:
                t_cont.clear();
                if (split(0, &buffer, nullptr, &t_cont, nullptr, "&message=") < 0)
                {
                    std::cout << "\033[1;31msplit() error\033[0m" << std::endl;
                    pthread_exit((void*)1);
                }
                buffer = t_cont.at(1);
                if (split(0, &buffer, nullptr, &t_cont, nullptr, "&sender=") < 0)
                {
                    std::cout << "\033[1;31msplit() error\033[0m" << std::endl;
                    pthread_exit((void*)1);
                }

                vec_echo.clear();
                t_str = "User ";
                std::copy(t_str.begin(), t_str.end(), std::back_inserter(vec_echo));
                std::copy(t_cont.at(1).begin(), t_cont.at(1).end(), std::back_inserter(vec_echo));
                t_str = " try to communicate through server. Message: ";
                std::copy(t_str.begin(), t_str.end(), std::back_inserter(vec_echo));
                std::copy(t_cont.at(0).begin(), t_cont.at(0).end(), std::back_inserter(vec_echo));
                break;
            case TEST_PROTOCOL:

                MyProtocolHeder prot;
                //print_buf("buffer 0:", &buffer);
                if(!std::memcpy(&prot, reinterpret_cast<void*>(buffer.data()) , sizeof(MyProtocolHeder)))
                {
                    std::cout << "TEST_PROTOCOL memcpy 1 error: " << errno << std::endl;
                };
                vec_echo.clear();

                //!this is message:
                std::copy(buffer.begin() + sizeof(MyProtocolHeder), buffer.begin() + sizeof(MyProtocolHeder) + prot.Size + 1, back_inserter(vec_echo));

                buffer.clear();
                std::copy(vec_echo.begin() , vec_echo.end() , back_inserter(buffer));

                switch(prot.Type)
                {
                    case TEXT:
                        mess = new Text(&buffer, &vec_echo);
                        break;
                    case PICTURE:
                        mess = new Picture(&buffer, &vec_echo);
                        break;
                    case ZIP:
                        mess = new Zip(&buffer, &vec_echo);
                        break;
                    default:
                        t_str = "Something gone wrong.";
                        vec_echo.clear();
                        std::copy(t_str.begin(), t_str.end(), std::back_inserter(vec_echo));
                        break;
                }
                ;break;
            default:
                vec_echo.clear();
                t_str = "Error: incorrect command.";
                std::copy(t_str.begin(), t_str.end(), std::back_inserter(vec_echo));
            }
            vec_echo.push_front('<');
            vec_echo.push_back('>');

#ifdef DEBUG
                std::cout << std::string(vec_echo.begin(), vec_echo.end()) << std::endl;
#endif

            if (sendall(sock, &vec_echo) < 0)//!echo to the client
            {
                std::cout << "\033[1;31msendall() error\033[0m" << std::endl;
                pthread_exit((void*)1);
            }
            vec_echo.clear();
        }
    }
    pthread_mutex_unlock(&mutex);
    //delete mess;
    pthread_exit((void*)0);
}
#endif // SERVER_H

