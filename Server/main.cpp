#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <iostream>
#include <errno.h>
#include <unistd.h>
#include <string>
#include <map>
#include <vector>
#include <fcntl.h>
#include <algorithm>
#include <set>
#include <ctime>
#include <stdlib.h>
#include <fstream>
#include <deque>
#include <cstring>
#include <pthread.h>
#include "mesage.h"
#include "server.h"

#ifdef UNIT_TEST
    #include "unittest.h"
#endif

int main()
{
#ifndef UNIT_TEST

    int sock;
    std::map<std::string, std::string> clients; //!map of clients clients["username"] = listen_port;
    std::map<std::string, long int> clients_uptime; //!map of online clients["username"] = uptime_in_seconds;
    std::set<int> speakers;

    containers Cont;
    Cont.clients = &clients;
    Cont.clients_uptime = &clients_uptime;
    Cont.speakers = &speakers;


    //!create thread to update map of clients
    pthread_t thread;
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    if (pthread_create( &thread, &attr, update_users_map, (void*)&Cont) != 0)
    {
        std::cout << "\033[1;31mpthread_create() error: \033[0m" << errno << std::endl;
        //pthread_exit((void*)1);
        return 1;
    }
    if (pthread_detach(thread) != 0)
    {
        std::cout << "\033[1;31mpthread_detach() error: \033[0m" << errno << std::endl;
        //pthread_exit((void*)1);
        return 2;
    }


    int listener;
    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(PORT_NUM_SERVER);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);

    listener = socket(AF_INET, SOCK_STREAM, 0);
    if (listener < 0)
    {
        std::cout << "\033[1;31msocket() eroor: \033[0m" << errno << std::endl;
        pthread_exit((void*)1);
    }

    fcntl(listener, F_SETFL, O_NONBLOCK);

    if (bind(listener, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        std::cout << "\033[1;31mbind() error: \033[0m" << errno << std::endl;
        //pthread_exit((void*)1);
        return 3;
    }
    listen(listener, 10);//!listen PORT_NUM_SERVER port

    if (pthread_mutex_init(&mutex, NULL) != 0)
    {
        std::cout << "\033[1;31mMutex init failed: \033[0m" << errno << std::endl;
        return 4;
    }

    while (1)
    {
        fd_set readset;//!container
        FD_ZERO(&readset);//!disable all flags in cont. readset
        FD_SET(listener, &readset);//!set flags in listener socket and push in readset

        for(std::set<int>::iterator it = speakers.begin(); it != speakers.end(); it++)
            FD_SET(*it, &readset);//!set socket's flags and push in readset

        timeval timeout;
        timeout.tv_sec = TIMEOUT_SERVER_SEC;
        timeout.tv_usec = 0;

        //pthread_mutex_lock(&mutex);
        int mx = std::max(listener, *std::max_element(speakers.begin(), speakers.end()));
        int result = select(mx + 1, &readset, NULL, NULL, &timeout);
        //pthread_mutex_unlock(&mutex);

        if (result <= 0)//!look for modifications on socket's in container readset
        {
            std::cout << "\033[1;31mselect() error: \033[0m" << errno << std::endl;
            //pthread_exit((void*)1);

            continue;
        }
        else
        {
            if (FD_ISSET(listener, &readset))//!if flag is enabled (if any changes was on socket)
            {
                sock = accept(listener, NULL, NULL);//!accept connection from client on socket sock (we don't need client's address)
                if(sock < 0)
                {
                    std::cout << "\033[1;31maccept() error: \033[0m" << errno << std::endl;
                    return 5;//pthread_exit((void*)1);
                }
                fcntl(sock, F_SETFL, O_NONBLOCK);//!set soket in non-blocking

                pthread_mutex_lock(&mutex);
                speakers.insert(sock);//!insert socket in container
                pthread_mutex_unlock(&mutex);

                Cont.readset = &readset;
                Cont.sock = &sock;

                pthread_t thread;
                pthread_attr_t attr;

                pthread_attr_init(&attr);

                if (pthread_create( &thread, &attr, listener_thread, (void*)&Cont) != 0)
                {
                    std::cout << "\033[1;31mpthread_create() error: \033[0m" << errno << std::endl;
                    return 6;
                }

                if (pthread_detach(thread) != 0)
                {
                    std::cout << "\033[1;31mpthread_detach() error: \033[0m" << errno << std::endl;
                    return 7;
                }
            }
        }
    }
    pthread_mutex_destroy(&mutex);
    std::cout << "\033[0;32mEnter any key to exit\033[0m" << std::endl;
    getchar();

    return 0;
#else
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
#endif
}
