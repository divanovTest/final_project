#ifndef MESSAGE
#define MESSAGE

#define PORT_NUM_SERVER 1234
#define PATH_FOR_IMAG_SAVE "/home/divanov/final_project/Client/saved.png"

#include <zlib.h>
#include <QByteArray>
#include <QBuffer>
#include <QImage>

enum ProtocolType
{
    TEXT = 1,
    PICTURE,
    ZIP
};

struct MyProtocolHeder
{
    ProtocolType Type;//!protocol type
    int Size;//!data length
};

void print_buf(const char* str, std::vector<unsigned char> *buf)
{
    int len = buf->size();
    std::cout << str << std::endl;
    for(int i = 0; i < len; i++)
    {
        printf("%02x ", (*buf)[i]);
    }
    std::cout << std::endl;
}

int sendall(int sock, std::string *buf, std::vector<unsigned char>* bufC = nullptr);
int recvall(int sock, std::string *buf);

/**
    @brief - abstract class
    @author Ivanov D
    @version 1.0
*/
class Message
{
public:
    //comment all
    MyProtocolHeder* m_protocol;
    std::vector<unsigned char> m_message;//!data
    std::vector<unsigned char> m_mesage_send;//!MyProtocolHeder and data
    int m_sock;
public:
    Message()
    {
    }

    virtual void print_message() = 0;
    virtual bool init() = 0;

    bool send_message(int port = -1)//make virtual
    {
        struct sockaddr_in addr;
        m_sock = socket(AF_INET, SOCK_STREAM, 0);
        if(m_sock < 0)
        {
            std::cout << "\033[1;31msocket() error: \033[0m" << errno << std::endl;
            return 0;
        }
        addr.sin_family = AF_INET;
        addr.sin_port = port > -1 ? htons(port) : htons(PORT_NUM_SERVER);
        addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
        if(connect(m_sock, (struct sockaddr *)&addr, sizeof(addr)) < 0)
        {
            std::cout << "\033[1;31mMessage connect() error: \033[0m" << errno << std::endl;
            return 0;
        }

        std::vector<unsigned char> message_send;
        message_send.push_back('<');
        message_send.push_back('3');
        message_send.push_back('=');
        std::copy(m_mesage_send.begin(), m_mesage_send.end(), std::back_inserter(message_send));
        message_send.push_back('>');

        //print_buf("message_S     = ", &message_send);
        sendall(m_sock, nullptr, &message_send);

        std::string message;
        recvall(m_sock, &message);
        close(m_sock);
        auto posl = message.find('<');
        auto posr = message.find('>');
        if(posl == std::string::npos || posr == std::string::npos)
        {
            message = "Incorrect command format.";
        }
        else
        {
            message = message.substr(posl + 1, posr - 1);
        }

        std::cout << "\033[0;32mEcho from server: \033[0m" << message << std::endl;

        m_sock = -1;
        return 1;
    }

    void test_message()//! parse m_mesage_send in MyProtocolHeder and message and print - make vitrual = 0 !!!
    {
        MyProtocolHeder t_prot;
        std::vector<unsigned char> t_message;
        std::memcpy(&t_prot, reinterpret_cast<void*>(m_mesage_send.data()), sizeof(MyProtocolHeder));
        t_message.reserve(t_prot.Size);
        std::copy(m_mesage_send.begin() + sizeof(MyProtocolHeder), m_mesage_send.begin() + sizeof(MyProtocolHeder) + t_prot.Size, back_inserter(t_message));

        std::cout << "Test message protocol: t_prot.Type: " + std::to_string(t_prot.Type) + "\nt_prot.Size: " + std::to_string(t_prot.Size) <<
                     "\nMessage: \n";
        for_each(t_message.begin(),t_message.end(),[](unsigned char ch)
        {
            std::cout << ch ;
        });
        std::cout << std::endl;
    }

    ~Message()
    {
        delete m_protocol;
    }

private:
    //Message() = delete;m_mesage_send
};

class Zip: public Message
{
public:
    Zip(std::vector<unsigned char>* buffer, std::deque<unsigned char>* vec_echo)
    {
        QByteArray* QBA1 = new QByteArray(reinterpret_cast<const char*>(buffer->data()), buffer->size());
        QByteArray QBA2 = qUncompress(*QBA1);
        vec_echo->clear();
        std::copy(QBA2.begin(), QBA2.end(), std::back_inserter(*vec_echo));

        std::copy(buffer->begin(), buffer->end(), std::back_inserter(m_message));
        init();
    }

    Zip(std::string message)
    {
        QByteArray a = message.c_str();
        QByteArray QBA = qCompress(a);
        m_message.clear();
        std::copy(QBA.begin(), QBA.end(), std::back_inserter(m_message));
        init();
    }

    bool init()
    {
        m_protocol = new MyProtocolHeder();
        m_protocol->Type = ZIP;
        m_protocol->Size = sizeof(unsigned char) * m_message.size();

        unsigned char* p_data = reinterpret_cast<unsigned char*>(m_protocol);
        for(int i = 0; i < (int)sizeof(MyProtocolHeder); i++)
        {
            m_mesage_send.push_back(p_data[i]);
        }
        for_each(m_message.begin(),m_message.end(),[this](unsigned char ch)
        {
            this->m_mesage_send.push_back(ch);
        });
        return 1;
    }

    void test_uncompress(QByteArray* QBA)
    {
        std::cout << "Test uncompression from QByteArray:\n";
        QByteArray t_QBA = qUncompress(*QBA);
        for(int i =0; i < t_QBA.size(); i++){
            std::cout << t_QBA[i];
        }
        std::cout << std::endl;
    }

    void test_uncompress(std::vector<unsigned char>* vec)
    {
        std::cout << "Test uncompression from vector:\n";
        QByteArray* t_QBA1 = new QByteArray(reinterpret_cast<const char*>(vec->data()), vec->size());
        QByteArray t_QBA2 = qUncompress(*t_QBA1);
        for(int i =0; i < t_QBA2.size(); i++){
            std::cout << t_QBA2[i];
        }
        std::cout << std::endl;
    }

    void print_message()
    {
        std::cout << "Hi. This is ZIP message: \n";
        for_each(m_message.begin(),m_message.end(),[](unsigned char ch)
        {
            std::cout << ch ;
        });
        std::cout << std::endl;
    }
private:
    Zip()= delete;
};

class Picture: public Message
{
public:
    Picture(std::vector<unsigned char>* buffer, std::deque<unsigned char>* vec_echo)
    {
        QByteArray* ba = new QByteArray(reinterpret_cast<const char*>(buffer->data()), buffer->size());
        QBuffer* bufferS = new QBuffer(ba);
        QImage image;
        image.loadFromData(bufferS->buffer()); // Load the image from buffer
        std::string t_str;
        if (image.isNull() || !(image.save(PATH_FOR_IMAG_SAVE)))
        {
            t_str = "The image is null. Something failed.";
        }
        else
        {
            t_str = "Picture was successfull saved";
        }
        vec_echo->clear();
        std::copy(t_str.begin(), t_str.end(), std::back_inserter(*vec_echo));

        std::copy( buffer->begin(), buffer->end(), std::back_inserter(m_message));
        init();
    }

    Picture(std::string picture_name)
    {
        //!load a QImage
        QImage image;
        if(!image.load(picture_name.c_str(), "PNG"))
        {
            std::cout << "Image load error" << std::endl;
        }
        QByteArray ba;
        QBuffer buffer(&ba);
        image.save(&buffer, "PNG");
        std::copy( ba.begin(), ba.end(), std::back_inserter(m_message));
        init();
    }

    bool init()
    {
        m_protocol = new MyProtocolHeder();
        m_protocol->Type = PICTURE;
        m_protocol->Size = sizeof(unsigned char) * m_message.size();
        unsigned char* p_data = reinterpret_cast<unsigned char*>(m_protocol);
        for(int i = 0; i < (int)sizeof(MyProtocolHeder); i++)
        {
            m_mesage_send.push_back(p_data[i]);
        }
        //test_picture();
        for_each(m_message.begin(),m_message.end(),[this](unsigned char ch)
        {
            this->m_mesage_send.push_back(ch);
        });
        return 1;
    }

    void test_picture()
    {
        QByteArray* ba = new QByteArray(reinterpret_cast<const char*>(m_message.data()), m_protocol->Size);
        print_buf("Picture:", &m_message);
        QBuffer* bufferS = new QBuffer(ba);
        QImage image;
        image.loadFromData(bufferS->buffer()); // Load the image from the receive buffer
        if (image.isNull())
        {
            std::cout <<  "The image is null. Something failed."<< std::endl;
        }
        else
        {
            std::cout << "The image is good :)" << std::endl;
        }
    }

    void print_message()
    {
        std::cout << "Hi. This is Picture message: \n";
        for_each(m_message.begin(),m_message.end(),[](unsigned char ch)
        {
            std::cout << ch ;
        });
        std::cout << std::endl;
    }
private:
    Picture()= delete;
};

class Text: public Message
{
public:
    Text(std::vector<unsigned char>* buffer, std::deque<unsigned char>* vec_echo)
    {
        std::string t_str = "Receive Text message: ";
        std::copy(buffer->begin(), buffer->end(), std::back_inserter(t_str));
        vec_echo->clear();
        std::copy(t_str.begin(), t_str.end(), std::back_inserter(*vec_echo));

        std::copy(buffer->begin(), buffer->end(), std::back_inserter(m_message));
        init();
    }

    Text(std::string message)
    {
        std::copy( message.begin(), message.end(), std::back_inserter(m_message));
        init();
    }

    bool init()
    {
        m_protocol = new MyProtocolHeder();
        m_protocol->Type = TEXT;
        m_protocol->Size = sizeof(unsigned char) * m_message.size() - 1;

        unsigned char* p_data = reinterpret_cast<unsigned char*>(m_protocol);
        for(int i = 0; i < (int)sizeof(MyProtocolHeder); i++)
        {
            m_mesage_send.push_back(p_data[i]);
        }
        for_each(m_message.begin(),m_message.end(),[this](unsigned char ch)
        {
            this->m_mesage_send.push_back(ch);
        });
        return 1;
    }

    void print_message()
    {
        std::cout << "Hi. This is Text message: \n";
        for_each(m_message.begin(),m_message.end(),[](unsigned char ch)
        {
            std::cout << ch ;
        });
        std::cout << std::endl;
    }
private:
    Text()= delete;
};

#endif // MESSAGE

