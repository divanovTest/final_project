/*!
    @file
    @brief header file , who contains testing class, mock objects and functions
*/
#ifndef UNITTEST_H
#define UNITTEST_H

#include <gmock/gmock.h>
#include <gtest/gtest.h>

using ::testing::_;

//override original functions
ssize_t send(int sockfd, const void *buf, size_t len, int flags)
{
    (void)sockfd;
    (void)buf;
    (void)flags;
    return len;
}

int bind(int sockfd, const struct sockaddr *addr, socklen_t addrlen)
{
    (void)sockfd;
    (void)addr;
    (void)addrlen;
    return 0;
}

int connect(int socket, const struct sockaddr *address, socklen_t address_len)
{
    (void)socket;
    (void)address;
    (void)address_len;
    return 0;
}

ssize_t recv(int sockfd, void *buf, size_t len, int flags)
{
    (void)flags;
    (void)len;
    (void)buf;
    (void)sockfd;
    return 0;
}

class FakeSocket
{
public:
    FakeSocket() {
        address.sin_family = AF_INET;
        address.sin_port = htons(PORT_NUM_SERVER);
        address.sin_addr.s_addr = htonl(INADDR_LOOPBACK);

        socket_ = socket(AF_INET, SOCK_STREAM, 0);
        if (socket_ == -1) {
            std::cout << "\033[1;31mGoogle mock test failed when calling FakeSocket class constructor. Error: \033[0m" << errno << std::endl;
        }
    }
    FakeSocket(int sock) : socket_(sock) {}
    bool Connect() {
        return (connect(socket_, (struct sockaddr*)&address, sizeof(address)) == 0);
    }
    FakeSocket* Accept() {
        int s = accept(socket_, NULL, NULL);
        if (s == -1) {
            std::cout << "\033[1;31mGoogle mock test failed when calling Accept method in FakeSocket class. Error: \033[0m" << errno << std::endl;
        }
        return new FakeSocket(s);
    }
    int* get_socket()
    {
        return &socket_;
    }
private:
    int socket_;
    struct sockaddr_in address;
};

class MockSock : public FakeSocket
{
public:
    MOCK_METHOD0(Connect, bool());
    MOCK_METHOD0(Accept, FakeSocket*());
    MOCK_METHOD0(get_socket, int*());
private:
    FakeSocket *sock;
};

TEST(sendallTest, sendall_1)
{
    FakeSocket sock;
    std::string buf = "test mesage";
    EXPECT_TRUE(sendall(*(sock.get_socket()), &buf) > -1);
}

TEST(recvallTest, recvall_1)
{
    FakeSocket sock;
    std::string buf = "";
    EXPECT_TRUE(recvall(*(sock.get_socket()), &buf) > -1);
}

TEST(MockSock, socketTest)
{
    MockSock sock;
    EXPECT_CALL(sock, Connect());
    sock.Connect();
    EXPECT_CALL(sock, Accept());//.Times(4);
    sock.Accept();
    EXPECT_CALL(sock, get_socket());
    int* n = sock.get_socket();
    //ASSERT_EQ(*n, 0);
}

#endif // UNITTEST_H

