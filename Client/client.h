/*!
    @file
    @brief header file , who contains main class Client and functions
*/
#ifndef CLIENT_H
#define CLIENT_H

#define BUFF_SIZE 512
#define PORT_NUM_SERVER 1234
#define PORT_TOP_RANGE 60000
#define PORT_LOWER_RANGE 50000
#define TIMEOUT_WAIT_THREAD_SEC 20
#define PORT_NUM_SERVER 1234
#define PATH_FOR_IMAG "/home/divanov/final_project/Client/test.png"
//#define UNIT_TEST
//#define DEBUG
//#define DEBUG_PROTOCOL

#include <vector>
#include <fcntl.h>
#include <algorithm>
#include <set>
#include <cstring>

#include "message.h"

enum command
{
    REFRESH_PORT = 0,
    REGISTRATION,
    SEND_MESSAGE,
    TEST_PROTOCOL
};

/*!
    Send message to client or server
    @param[in] sock listener socket of server or another client
    @param[in] buf pointer for message string
    @return number of bytes sent or -1 if get some error

    This function use posix function send and pointer for message string in loop still message will not sending
*/
int sendall(int sock, std::string *buf, std::vector<unsigned char>* bufC)
{
    int res = 0;
    const char* msgPtr = buf != nullptr ? buf->c_str() : reinterpret_cast<char*>(bufC->data());

    int len = buf != nullptr ? sizeof(char) * buf->length() : bufC->size();
    int offset = 0;

    while ((res = send(sock, msgPtr + offset, len/*(len < BUFF_SIZE ? len : BUFF_SIZE)*/, 0)))
    {
        if(res < 0)
        {
            std::cout << "Error send:" << errno << std::endl;
            break;
        }
        offset += res;
        len -= res;
        if(len < BUFF_SIZE || res < BUFF_SIZE || res == len)break;

    }
    return (res == -1 ? -1 : offset);
}

/*!
    Receive message from another client,server or himself
    @param[in] sock this client listen socket
    @param[in,out] buf pointer for message string - in this string function write received message in loop
    @return number of bytes received or -1 if get some error

    this function use posix function recv and pointer for message string still message will not read from socket in loop
*/
int recvall(int sock, std::string *buf)
{
    int res = 0;
    int total = 0;
    char recv_buffer[BUFF_SIZE];
    *buf = "";

    while ((res = recv(sock, recv_buffer, BUFF_SIZE, 0)) > 0)
    {
        *buf += recv_buffer;
        total += res;
        if(res < BUFF_SIZE)break;
    }
    return (res == -1 ? -1 : total);
}



void* listener_thread(void* struct_address);
/**
    @brief - main class
    @author Ivanov D
    @version 1.0

    this class used for work ...

 */
class Client
{
public:
    Client() : m_listener(-1), m_sock(-1)
    {

    }
    //!send message
    void send_message(std::string &message, int port = -1)
    {
        struct sockaddr_in addr;
        m_sock = socket(AF_INET, SOCK_STREAM, 0);
        if(m_sock < 0)
        {
            std::cout << "\033[1;31msocket() error: \033[0m" << errno << std::endl;
            return;
        }
        addr.sin_family = AF_INET;
        addr.sin_port = port > -1 ? htons(port) : htons(PORT_NUM_SERVER);
        addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
        if(connect(m_sock, (struct sockaddr *)&addr, sizeof(addr)) < 0)
        {
            std::cout << "\033[1;31mconnect() error: \033[0m" << errno << std::endl;
            return;
        }
        sendall(m_sock, &message);
        message.clear();
        recvall(m_sock, &message);
        close(m_sock);
        auto posl = message.find('<');
        auto posr = message.find('>');
        if(posl == std::string::npos || posr == std::string::npos)
        {
            //message = "Incorrect command format.";
        }
        else
        {
            message = message.substr(posl + 1, posr - 1);
        }
//    #ifdef DEBUG
//        std::cout << "\033[0;32mEcho from server: \033[0m" << message << std::endl;
//    #endif
        m_sock = -1;
        return;
    }
    //!split string and fill stl containers
    int split(std::string splitter, std::string *str, std::map<std::string, std::string> *container, std::vector<std::string> *vec)
    {
        if(str->empty())return 1;
        int pos = str->find(splitter);
        std::string head = str->substr(0, pos);
        std::string tail = str->substr(pos + splitter.size(), str->size() - 1);

        if(head.empty() || tail.empty())return 2;

        if (container != nullptr)
        {
            if(container->count(head))
            {
                 container->at(head) = tail;
            }
            else
            {
                container->insert(std::pair<std::string, std::string>(head, tail));
            }
        }
        if (vec != nullptr)
        {
            vec->clear();
            vec->push_back(head);
            vec->push_back(tail);
        }
        return 0;
    }
    //!the same
    void split(char splitter1, char splitter2, std::vector<unsigned char>* str, std::map<std::string, std::string> *container)
    {
        if(str->empty())return;
        std::vector<unsigned char>::iterator it = str->begin();
        while(*it != splitter1 && it != str->end())it++;

        std::string head = std::string(str->begin(), it);
        std::string tail = std::string(it + 1, str->end());

        if(head.size() < 1)return;

        std::string::iterator it2 = head.begin();
        while(*it2 != splitter2 && it2 != head.end())it2++;

        if(it2 + 1 >= head.end())return;
        std::string subhead = std::string(head.begin(), it2);
        std::string subtail = std::string(it2 + 1, head.end());

        if(container->count(subhead))
        {
             container->at(subhead) = subtail;
        }
        else
        {
            container->insert(std::pair<std::string, std::string>(subhead, subtail));
        }
        if(tail.size() < 1)return;

        str->clear();
        for_each(tail.begin(), tail.end(), [str](unsigned char ch)
        {
            str->push_back(ch);
        });
        //std::copy(tail.begin(), tail.end(), std::back_inserter(str));

        split(splitter1, splitter2, str, container);
    }
    //!create thread for listening messages
    int try_to_listen()
    {
        pthread_attr_init(&m_attr);

        if (pthread_create( &m_thread, &m_attr, listener_thread, (void*)this) != 0)
        {
            std::cout << "\033[1;31mpthread_create() error: \033[0m" << errno << std::endl;
            return 1;
        }

        if (pthread_detach(m_thread) != 0)
        {
            std::cout << "\033[1;31mpthread_detach() error: \033[0m" << errno << std::endl;
            return 1;
        }
        return 0;
    }
    //!add message in queue when user is busy
    void add_queue_messages(std::string* str)
    {
        m_queue_of_messages.push_back(*str);
    }
    //!show messages when user are not busy
    void show_queue_messages()
    {
        for(auto b : m_queue_of_messages)std::cout << b << std::endl;
        m_queue_of_messages.clear();
    }
    //!return stl container where keep list of online users with opened them ports
    std::map<std::string, std::string>* get_user_data()
    {
        return &m_users_data;
    }
    //!return m_username
    std::string* get_username()
    {
        return &m_username;
    }
    //!return address from which user listen incoming connections
    struct sockaddr_in* get_addr()
    {
        return &m_listen_addr;
    }
    //!return server socket
    int* get_soket()
    {
        return &m_sock;
    }
    //!return listener socket
    int* get_listener_soket()
    {
        return &m_listener;
    }

private:
    pthread_t m_thread;
    pthread_attr_t m_attr;
    int m_listener, m_sock;//!<m_listener - socket to listen incoming connections, m_sock - soket for server connection
    std::vector<std::string> m_queue_of_messages;//!<queue of incoming messages.
    std::map<std::string, std::string> m_users_data;//!<container where keep list of online users with opened them ports
    std::string m_username;
    struct sockaddr_in m_listen_addr;//!<address to listen incoming connections
};

/*!
    Thread for update list of users and used them listening ports
    @param[in] client poiner for object of class Client

    This function is used in thread.
    In infinity loop this function send client's listener socket (m_listener field in clas Client) and receive mesage from server and update stl container users_data of class Client from server
    This function is called from main
*/
void* get_list_of_users_thread(void* client)
{
    Client* cl = (Client*)client;
    while(1)
    {

        std::string message = *(cl->get_username()) + ":" + std::to_string(ntohs(cl->get_addr()->sin_port));//"<" + std::to_string(REFRESH_PORT) + "=" + *(cl->get_username()) + ":" + std::to_string(ntohs(cl->get_addr()->sin_port)) + ">";
        //cl->send_message(message);
        Text* mess = new Text(message);
        mess->send_message(-1, '0');

#ifdef DEBUG
        std::vector<unsigned char>* t_mess = mess->ret_mess();
        std::cout << "\033[1;32mSuccessfull.\033[0m \033[0;32mReceive list of users data: \033[0m" << std::string(t_mess->begin(), t_mess->end()) << std::endl;
#endif
        cl->get_user_data()->clear();
        cl->split('|', ':', mess->ret_mess()/* &message*/, cl->get_user_data());
        delete mess;
        sleep(TIMEOUT_WAIT_THREAD_SEC);
    }
    pthread_exit((void*)0);
}

/*!
    Thread for receive incoming messages from another clients or himself
    @param[in] client poiner for object of class Client

    This function is used in thread.
    In infinity loop this function listen socket defined in class Client as m_listener
    This function is called from Client::try_to_listen()
*/
void* listener_thread(void* client)
{
    std::string buffer = "";
    Client* cl = (Client*)client;
    int* listener = cl->get_listener_soket();
    int* sock = cl->get_soket();
    std::vector<std::string> t_cont;

    *listener = socket(AF_INET, SOCK_STREAM, 0);
    if (*listener < 0)
    {
        std::cout << "\033[1;31msocket() eroor: \033[0m" << errno << std::endl;
        pthread_exit((void*)1);
    }

    fcntl(*listener, F_SETFL, O_NONBLOCK);

    cl->get_addr()->sin_family = AF_INET;
    cl->get_addr()->sin_addr.s_addr = 0;
    cl->get_addr()->sin_addr.s_addr = INADDR_ANY;
    int port_num = PORT_LOWER_RANGE;
    do{
        cl->get_addr()->sin_port = htons(port_num);//!0;
        if (bind(*listener, (struct sockaddr *)(cl->get_addr()), sizeof(struct sockaddr_in)) < 0)
        {
            port_num++;
        }
        else
        {
            break;
        }

    }
    while(port_num < PORT_TOP_RANGE);

    listen(*listener, 10);//!queue = 10

    std::set<int> speakers;
    speakers.clear();

    while (1)
    {
        fd_set readset;
        FD_ZERO(&readset);
        FD_SET(*listener, &readset);

        for(std::set<int>::iterator it = speakers.begin(); it != speakers.end(); it++)
            FD_SET(*it, &readset);

        timeval timeout;
        timeout.tv_sec = 1500;
        timeout.tv_usec = 0;

        int mx = std::max(*listener, *std::max_element(speakers.begin(), speakers.end()));
        if (select(mx + 1, &readset, NULL, NULL, &timeout) <= 0)
        {
            std::cout << "\033[1;31mselect() error: \033[0m" << errno << std::endl;
            pthread_exit((void*)1);
        }

        if(FD_ISSET(*listener, &readset))
        {
            *sock = accept(*listener, NULL, NULL);
            if(*sock < 0)
            {
                std::cout << "\033[1;31maccept() error: \033[0m" << errno << std::endl;
                pthread_exit((void*)1);
            }
            fcntl(*sock, F_SETFL, O_NONBLOCK);
            speakers.insert(*sock);
        }

        for(std::set<int>::iterator it = speakers.begin(); it != speakers.end(); it++)
        {
            if(FD_ISSET(*it, &readset))
            {
                if(recvall(*it, &buffer) <= 0)
                {
                    close(*it);
                    speakers.erase(*it);
                    continue;
                }
                t_cont.clear();
                auto posl = buffer.find('<');
                auto posr = buffer.find('>');
                if(posl == std::string::npos || posr == std::string::npos)
                {
                    buffer = "Incorrect command format.";
                }
                else
                {
                    buffer = buffer.substr(posl + 1, posr - 1);
                }
                if (cl->split("&sender=", &buffer, nullptr, &t_cont) < 0)
                {
                    std::cout << "\033[1;31msplit() error\033[0m" << std::endl;
                    pthread_exit((void*)1);
                }
                //!echo to the client
                buffer = "<OK>";
                if(sendall(*sock, &buffer) < 0)
                {
                    std::cout << "\033[1;31msendall() error\033[0m" << std::endl;
                    pthread_exit((void*)1);
                }
                //!t_cont.at(1)  - sender
                //!t_cont.at(0) - message
                buffer =  "\n***************************\n\033[1;32mSender: \033[0m" + t_cont.at(1) + "\n\033[1;32mMessage: \033[0m" + t_cont.at(0) + "\n***************************\n";
#ifdef DEBUG
                std::cout << buffer << std::endl;
#endif
                cl->add_queue_messages(&buffer);
            }
        }
    }
    pthread_exit(0);
}

#endif //! CLIENT_H

