
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <iostream>
#include <errno.h>
#include <unistd.h>
#include <string>
#include <pthread.h>
#include <map>
#include <arpa/inet.h>
#include "client.h"
#ifdef UNIT_TEST
    #include "unittest.h"
#endif

int main()
{
#ifndef UNIT_TEST
    Client cl;
    cl.try_to_listen();
    Message* mess4;

    //!try to register on server
    std::string message = "";
    do
    {
        std::cout << ((message == "Bad username") ? "\033[0;31mUsername already exist, please enter another name: \033[0m" :
                                     "\033[0;32mPlease enter username (do not use next symbols: '>', '<', '=' ): \033[0m");
        std::cin >> *(cl.get_username());
        message = "<" + std::to_string(REGISTRATION) + "=" + *(cl.get_username()) + ">";
        cl.send_message(message);
    }
    while(message != "OK");
    std::string opponent = "";

    //!thread to update user\s data;
    std::cout << "\033[1;32mSuccessfull registration.\033[0m\n\033[0;32mSending client's listener port...\033[0m" << std::endl;
    pthread_t thread;
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    //!create thread for listening messages
    if (pthread_create( &thread, &attr, get_list_of_users_thread, (void*)&cl) != 0)
    {
        std::cout << "\033[1;31mpthread_create() error: \033[0m" << errno << std::endl;
        return 1;
    }
    if (pthread_detach(thread) != 0)
    {
        std::cout << "\033[1;31mpthread_detach() error: \033[0m" << errno << std::endl;
        return 1;
    }

    char code = '3';
    while (1)
    {
        std::cout << "\033[1;32mPlease select command:\033[0m\n\r\033[0;34m1. Отобразить список доступных в данный момент собеседников.\033[0m\n\
                        \r\033[0;36m2. Написать и отправить сообщение определенному собеседнику.\033[0m\n\
                        \r\033[0;37m3. Завершить работу клиента.\033[0m\n\
                        \r\033[0;37m4. Тестовое сообщение.\033[0m\n";
        std::cin >> code;
        if(code == '1')
        {
            std::cout << "\033[1;32mList of user's:\033[0m" << std::endl;
            for(auto it = cl.get_user_data()->cbegin(); it != cl.get_user_data()->cend(); ++it)
            {
                std::cout << it->first << std::endl;
            }
        }
        else if(code == '2')
        {
            std::cout << "\033[0;32mPlease select opponent to communicate: \033[0m";
            std::cin >> opponent;
            auto pos = cl.get_user_data()->find(opponent);
            if(pos != cl.get_user_data()->end())
            {
                std::cout << "\033[0;32mPlease type message here: \033[0m";
                std::getline(std::cin >> std::ws, message);
                message = "<" + message + "&sender=" + *(cl.get_username()) + ">";

                cl.send_message(message, std::stoi((*(cl.get_user_data()))[opponent]));
                if(message != "OK")
                {
                    std::cout << "\033[1;31mSomething went wrong.\033[0m" << std::endl;
                }
            }
            else
            {
                std::cout << "\033[0;31mUsername is not exist\033[0m" << std::endl;
            }
        }
        else if(code == '3')
        {
            std::cout << "\033[1;31mExit.\033[0m" << std::endl;
            break;
        }
        else
        {
            std::cout << "\033[0;32mPlease select message type: \033[0m\n\
                        \r\033[0;36m1. Text.\033[0m\n\
                        \r\033[0;36m2. Picture.\033[0m\n\
                        \r\033[0;36m3. Zip.\033[0m\n";
            int switcher = 1;
            std::cin >> switcher;
            ProtocolType pt = (ProtocolType)switcher;
            std::string t_str;
            switch(pt)
            {
            case TEXT:
                std::cout << "Please enter Text message: ";
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                std::getline(std::cin, t_str);
                mess4 = new Text(t_str);
#ifdef DEBUG_PROTOCOL
//                mess4->test_message();
//                mess4->print_message();
#endif
                mess4->send_message();
                ;break;
            case PICTURE:
                mess4 = new Picture(PATH_FOR_IMAG);
#ifdef DEBUG_PROTOCOL
//                mess4->test_message();
//                mess4->print_message();
#endif
                mess4->send_message();
                ;break;
            case ZIP:
                std::cout << "Please enter Zip message: ";
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                std::getline(std::cin, t_str);
                mess4 = new Zip(t_str);
#ifdef DEBUG_PROTOCOL
//                mess4->test_message();
//                mess4->print_message();
#endif
                mess4->send_message();
                ;break;
            default:
                std::cout << "Error: incorrect protocol type" << std::endl;
            }

        }
        //!show messages from queue
        cl.show_queue_messages();
    }

    return 0;
#else
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
#endif
}

