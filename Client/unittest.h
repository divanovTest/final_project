/*!
    @file
    @brief header file , who contains testing class, mock objects and functions
*/
#ifndef UNITTEST_H
#define UNITTEST_H

#include <gmock/gmock.h>
#include <gtest/gtest.h>

using ::testing::_;

/*!
    @brief overrided original posix function
    @param[in] sockfd, buf, flags is not used parameters
    @return len - parameter of this function

    This function overlaped for unit testing
*/
ssize_t send(int sockfd, const void *buf, size_t len, int flags)
{
    (void)sockfd;
    (void)buf;
    (void)flags;
    return len;
}

/*!
    @brief overrided original posix function
    @param[in] sockfd, addr, addrlen is not used parameters
    @return always return zero

    This function overlaped for unit testing
*/
int bind(int sockfd, const struct sockaddr *addr, socklen_t addrlen)
{
    (void)sockfd;
    (void)addr;
    (void)addrlen;
    return 0;
}

/*!
    @brief overrided original posix function
    @param[in] socket, address, address_len is not used parameters
    @return always return zero

    This function overlaped for unit testing
*/
int connect(int socket, const struct sockaddr *address, socklen_t address_len)
{
    (void)socket;
    (void)address;
    (void)address_len;
    return 0;
}

/*!
    @brief overrided original posix function
    @param[in] sockfd, buf, len, flags is not used parameters
    @return always return zero

    This function overlaped for unit testing
*/
ssize_t recv(int sockfd, void *buf, size_t len, int flags)
{
    (void)flags;
    (void)len;
    (void)buf;
    (void)sockfd;
    return 0;
}

/**
    @brief - class for socket testing
    @author Ivanov D
    @version 1.0

    This is parent class.

 */
class FakeSocket
{
public:
    //!this constructor init m_address and m_socket
    FakeSocket() {
        m_address.sin_family = AF_INET;
        m_address.sin_port = htons(PORT_NUM_SERVER);
        m_address.sin_addr.s_addr = htonl(INADDR_LOOPBACK);

        m_socket = socket(AF_INET, SOCK_STREAM, 0);
        if (m_socket == -1) {
            std::cout << "\033[1;31mGoogle mock test failed when calling FakeSocket class constructor. Error: \033[0m" << errno << std::endl;
        }
    }
    FakeSocket(int sock) : m_socket(sock) {}
    /*!
        @brief emulate posix function connect
        @return true if connected or false if can't connect
    */
    bool Connect() {
        return (connect(m_socket, (struct sockaddr*)&m_address, sizeof(m_address)) == 0);
    }
    /*!
        @brief emulate posix function accept
        @return pointer for object of class FakeSocket
    */
    FakeSocket* Accept() {
        int s = accept(m_socket, NULL, NULL);
        if (s == -1) {
            std::cout << "\033[1;31mGoogle mock test failed when calling Accept method in FakeSocket class. Error: \033[0m" << errno << std::endl;
        }
        return new FakeSocket(s);
    }
    //!@return pointer to the socket m_socket
    int* get_socket()
    {
        return &m_socket;
    }
private:
    int m_socket;
    struct sockaddr_in m_address;
};

/**
    @brief - class for socket mock testing
    @author Ivanov D
    @version 1.0

    This class derived from FakeSocket.
    In this class create mock methods, who called from TEST(MockSock, socketTest)
 */
class MockSock : public FakeSocket
{
public:
    MOCK_METHOD0(Connect, bool());
    MOCK_METHOD0(Accept, FakeSocket*());
    MOCK_METHOD0(get_socket, int*());
private:
    FakeSocket *m_sock;
};

//!Test for sendall function
TEST(sendallTest, sendall_1)
{
    FakeSocket sock;
    std::string buf = "test mesage";
    EXPECT_TRUE(sendall(*(sock.get_socket()), &buf) > -1);
}

//!Test for recvall function
TEST(recvallTest, recvall_1)
{
    FakeSocket sock;
    std::string buf = "";
    EXPECT_TRUE(recvall(*(sock.get_socket()), &buf) > -1);
}

//!Test for mock methods, created in class MockSock
TEST(MockSock, socketTest)
{
    MockSock sock;
    EXPECT_CALL(sock, Connect());
    sock.Connect();
    EXPECT_CALL(sock, Accept());//.Times(4);
    sock.Accept();
    EXPECT_CALL(sock, get_socket());
    int* n = sock.get_socket();
    //ASSERT_EQ(*n, 0);
}

#endif // UNITTEST_H

