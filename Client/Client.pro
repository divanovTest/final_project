QT += core gui

TARGET = Client
CONFIG += console
CONFIG -= app_bundle
CONFIG += c++11

TEMPLATE = app

SOURCES += main.cpp

INCLUDEPATH += /usr/include/gtest
INCLUDEPATH += /usr/include/gmock

LIBS += /usr/src/gtest/libgtest.a

LIBS += /usr/src/gmock/build/libgmock.a

HEADERS += \
    client.h \
    unittest.h \
    message.h

